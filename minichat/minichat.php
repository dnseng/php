<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>MiniChat APP</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>
  	
    <div class="app">
    	<div class="app-title">
  			<h1>Minichat App</h1>
  		</div>
  		<!-- Formulaire -->
    	<div class="formulaire">
	    	<form action="minichat_post.php" method = "POST">
	    		<p class="data">
		    		<label for="pseudo">Pseudo </label> : <input type="text" name="pseudo" id="pseudo"/> <br/>
		    		<label for="message">Message </label> : <input type="text" name="message" id="message"/> <br/>
	    		</p>
	    	
	    		<p class="submit">
	    			<input type="submit" value="Envoyer"/>
	    		</p>
	    	</form>
    	</div>
    	<!-- Connexion à la base de données -->
    	<div class="data-recup">
    		<?php 
    		// Connexion à la base de données
    		try 
    		{
    			$bdd = new PDO('mysql:host=localhost;dbname=minichat;charset=utf8', 'root', '');
    		}
    		catch(Exception $e)
    		{
    			die('Erreur : ' .$e -> getMessage());
    		}

    		/*echo '<p>connexion réussie !</p>';*/

    		// récupération des messages
    		$answer = $bdd -> query('SELECT pseudo, message, date FROM chat ORDER BY ID DESC');

    		/*echo '<p>requête réussie !</p>';*/

    		// Affichage de chaque message (toutes les données sont protégées pat htmlspecialchars)

    		while ($data = $answer->fetch()) 
    		{

    			echo '<p><strong>' . htmlspecialchars($data['pseudo']) . '</strong> [ <em style=color:red;>' . htmlspecialchars($data['date']) . '</em> ] : ' . htmlspecialchars($data['message']) . '<hr> </p>';
    		}

    		$answer -> closeCursor();
    	?>
    	</div>
    

    </div>
    	
    







    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-W8fXfP3gkOKtndU4JGtKDvXbO53Wy8SZCQHczT5FMiiqmQfUpWbYdTil/SxwZgAN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    -->
  </body>
</html>